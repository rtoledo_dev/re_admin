class AddFieldsToCompanies < ActiveRecord::Migration
  def change
    change_table :companies do |t|
      t.string :address_zipcode, size: 100
      t.string :address
      t.string :address_number, size: 100
      t.string :address_complement
      t.string :address_neighborhood, size: 100
      t.string :address_city, size: 100
      t.string :address_state, size: 100
      t.string :address_country, size: 100
      t.string :twitter
      t.string :facebook
      t.string :google_plus
      t.string :linkedin
      t.attachment :twitter_logo
      t.attachment :facebook_logo
      t.attachment :google_plus_logo
      t.attachment :linkedin_logo
    end
  end
end