class CreateRealEstates < ActiveRecord::Migration
  def change
    create_table :real_estates do |t|
      t.references :company
      t.references :real_estate_image
      t.string :real_estate_type, default: :house
      t.string :title
      t.string :short_description
      t.string :address_zipcode, size: 100
      t.string :address
      t.string :address_number, size: 100
      t.string :address_complement
      t.string :address_neighborhood, size: 100
      t.string :address_reference, size: 100
      t.string :address_region, size: 100
      t.string :address_city, size: 100
      t.string :address_state, size: 100
      t.string :address_country, size: 100
      t.float :selling_price
      t.float :comission_price
      t.boolean :sold
      t.boolean :active

      t.string :owner_name
      t.string :owner_phone
      t.string :owner_mobile_phone
      t.string :owner_agent_name
      t.string :owner_agent_phone
      t.string :owner_agent_mobile_phone

      t.boolean :gated
      t.integer :rooms
      t.integer :bathrooms
      t.integer :terrain_area
      t.integer :build_area
      t.boolean :fireplace
      t.boolean :pool
      t.boolean :bbq_area
      t.boolean :kennel
      t.boolean :dependencies
      t.boolean :homemade_house
      t.boolean :turkish_bath
      t.boolean :game_room
      t.timestamps
    end
  end
end
