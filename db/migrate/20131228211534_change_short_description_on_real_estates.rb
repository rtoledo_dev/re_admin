class ChangeShortDescriptionOnRealEstates < ActiveRecord::Migration
  def up
    change_column :real_estates, :short_description, :text
  end

  def down
    change_column :real_estates, :short_description, :string
  end
end
