class AddMasterCompanyIdToUsers < ActiveRecord::Migration
  def change
    add_column :users, :master_company_id, :integer
  end
end
