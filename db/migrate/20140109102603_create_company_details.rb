class CreateCompanyDetails < ActiveRecord::Migration
  def change
    create_table :company_details do |t|
      t.references :company, index: true
      t.references :detail, index: true
      t.string :value

      t.timestamps
    end
  end
end
