class AddMasterToCompanies < ActiveRecord::Migration
  def change
    add_column :companies, :master, :boolean, default: false
  end
end
