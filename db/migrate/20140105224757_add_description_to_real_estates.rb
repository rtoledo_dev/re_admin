class AddDescriptionToRealEstates < ActiveRecord::Migration
  def change
    add_column :real_estates, :description, :text
  end
end
