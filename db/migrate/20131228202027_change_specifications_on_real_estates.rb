class ChangeSpecificationsOnRealEstates < ActiveRecord::Migration
  def up
    change_column :real_estates, :rooms, :string
    change_column :real_estates, :bathrooms, :string
  end

  def down
    change_column :real_estates, :rooms, :integer
    change_column :real_estates, :bathrooms, :integer
  end
end
