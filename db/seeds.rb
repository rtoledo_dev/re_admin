# encoding: utf-8
Company.new(name: 'Company Example').save(validate: false)
company = Company.first
company.update_attribute(:company, company)

User.new(master_company: Company.first,
  company: Company.first, email: 'administrador@forallimoveis.com.br',
  password: 'raposa123', password_confirmation: 'raposa123',
  name: 'Administrador', sys_admin: true, master: true).save

User.new(master_company: Company.first,
  company: Company.first, email: 'rodrigo@rtoledo.inf.br',
  password: 'hahire45', password_confirmation: 'hahire45',
  name: 'Rodrigo Toledo', sys_admin: true, master: true).save

%w(Administração Venda Permuta Financiamento Locação Avaliação Compra Temporada).each do |goal|
  Goal.create(name: goal)
end

[["E-mail","email"],["Telefone","phone"],["Celular","mobile_phone"],["Contato","contact"]].each do |grouped|
  Detail.create(name: grouped[0], code: grouped[1])
end
