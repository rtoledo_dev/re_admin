<?php
class ApiConnection
{
  public $loginResult;

  public function __construct()
  {
    $this->user = '[EMAIL_USER]';
    $this->password = '[PASS_USER]';
    $this->api_url = '[URL_OF_SERVICE]'; // something like http://www.example.com/api
  }

  public function doLogin()
  {
    $fields = array(
      'email' => $this->user,
      'password' => $this->password
    );
    $ch = curl_init();
    curl_setopt($ch,CURLOPT_URL, $this->api_url."/login");
    curl_setopt($ch,CURLOPT_POST, count($fields));
    curl_setopt($ch,CURLOPT_POSTFIELDS, $this->paramsToQuery($fields));
    curl_setopt($ch,CURLOPT_RETURNTRANSFER, 1);
    $this->loginResult = curl_exec($ch);
    curl_close($ch);
  }

  private function paramsToQuery($params)
  {
    $query = array();
    foreach($params as $key=>$value){
      $query[] = $key.'='.$value;
    }
    return join('&',$query);
  }

  public function token()
  {
    $token = json_decode($this->loginResult);
    return $token->auth_token;
  }

  /**
   * You can change this function or clone to use parameters
   */
  public function consumeAction($action)
  {
    $ch = curl_init();
    curl_setopt($ch,CURLOPT_URL, $this->api_url."/$action?auth_token=".$this->token());
    curl_setopt($ch,CURLOPT_RETURNTRANSFER, 1);
    $responseAction = curl_exec($ch);
    $responseAction = json_decode($responseAction);
    curl_close($ch);
    return $responseAction;
  }
}
