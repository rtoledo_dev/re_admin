class User < ActiveRecord::Base
  TYPES = {agent: 'Corretor', client: 'Cliente', commercial: 'Comercial',
    administrative: 'Administrativo'}

  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable,
         :token_authenticatable
  belongs_to :company
  belongs_to :master_company, class_name: "Company"
  has_many :user_details
  accepts_nested_attributes_for :user_details, allow_destroy: true, reject_if: proc { |attributes| attributes['value'].blank? || attributes['detail_id'].blank? }
  validates_presence_of :company_id, :master_company_id, :name, :user_type
  has_paper_trail

  def master?
    self.master
  end

  def sys_admin?
    self.sys_admin
  end

  def user_type_full_text
    User::TYPES[self.user_type.to_sym]
  end
end
