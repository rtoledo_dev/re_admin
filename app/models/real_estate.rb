class RealEstate < ActiveRecord::Base
  TYPES = {apartment: 'Apartamento', house: 'Casa', ground: 'Terreno',
    farm: 'Fazenda', hostel: 'Pousada', horse_farm: 'Haras', shop: 'Loja',
    restaurant: 'Restaurante', comercial_room: 'Sala', sitio: 'Sítio'}
  has_paper_trail

  has_many :goal_real_estates
  has_many :goals, through: :goal_real_estates
  has_many :real_estate_images, order: 'master desc'
  belongs_to :real_estate_image
  belongs_to :company

  validates :title, :short_description, :real_estate_type, :company, presence: true

  usar_como_dinheiro :selling_price, :comission_price, :sold_price, :terrain_area, :build_area

  def last_modification_by
    User.find(self.versions.last.whodunnit).try(:name)
  end

  def address_country
    'Brasil'
  end

  def real_estate_type_full_text
    RealEstate::TYPES[self.real_estate_type.to_sym]
  end

  def to_api(simple = false)
    response = {
      id: self.id,
      title: self.title,
      short_description: self.short_description,
      type: self.real_estate_type_full_text,
      sold_at: self.sold_at.to_s,
      sold_price: self.sold_price.to_s,
      selling_price: self.selling_price.to_s,
      comission_price: self.comission_price.to_s,
      image: self.real_estate_image ? self.real_estate_image.image.url : '',
      updated_at: self.updated_at.to_s,
      created_at: self.created_at.to_s,
      last_modification_by: self.last_modification_by
    }
    unless simple
      response = response.merge({
        address: {
          zipcode: self.address_zipcode.to_s,
          address: self.address.to_s,
          number: self.address_number.to_s,
          complement: self.address_complement.to_s,
          neighborhood: self.address_neighborhood.to_s,
          reference: self.address_reference.to_s,
          region: self.address_region.to_s,
          city: self.address_city.to_s,
          state: self.address_state.to_s,
          country: self.address_country.to_s
        },
        specifications: {
          gated: self.gated.to_s,
          rooms: self.rooms.to_s,
          bathrooms: self.bathrooms.to_s,
          terrain_area: self.terrain_area.to_s,
          build_area: self.build_area.to_s,
          fireplace: self.fireplace.to_s,
          pool: self.pool.to_s,
          bbq_area: self.bbq_area.to_s,
          kennel: self.kennel.to_s,
          dependencies: self.dependencies.to_s,
          homemade_house: self.homemade_house.to_s,
          turkish_bath: self.turkish_bath.to_s,
          game_room: self.game_room.to_s,
        },
        owner: {
          name: self.owner_name.to_s,
          phone: self.owner_phone.to_s,
          mobile_phone: self.owner_mobile_phone.to_s,
        },
        agent: {
          name: self.owner_agent_name.to_s,
          phone: self.owner_agent_phone.to_s,
          mobile_phone: self.owner_agent_mobile_phone.to_s,
        },
        images: images_url
      })
    end
    response
  end

  def images_url
    self.real_estate_images.collect{|t| t.image.url}
  end
end
