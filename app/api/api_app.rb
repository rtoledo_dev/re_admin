# encoding: utf-8
class ApiApp < Sinatra::Base
  register Sinatra::Contrib
  helpers Sinatra::JSON
  use ActionDispatch::Session::CookieStore, key: '_re_admin_session',
    secret: "709da9da1990e21177cb1d74d016719419ce8c621d940805a53e5076022342224326f5407f598107695df443c7ffb7a5410edaa88ba3d784eb5267d8fbb127e6"

  configure :production, :development do
    enable :logging
  end

  post '/login' do
    warden_handler.authenticate!
    if warden_handler.authenticated?
      warden_handler.user.ensure_authentication_token!
      response = { success: true, auth_token: warden_handler.user.authentication_token,
        email: warden_handler.user.email }
      json response
    else
      invalid_login_attempt
    end
  end

  get "/real_estates/:id" do
    check_authentication
    real_estate = warden_handler.user.master_company.all_real_estates.find_by_id(params[:id])
    not_found unless real_estate
    
    json real_estate.to_api
  end

  get "/real_estates" do
    check_authentication
    real_estates = warden_handler.user.master_company.all_real_estates.collect{|t| t.to_api(true) }
    not_found if real_estates.blank?
    
    json real_estates
  end

  def warden_handler
    env['warden']
  end

  def check_authentication
    warden_handler.authenticate!
  end

  not_found do
    json error_response("Requested resource was not found.", 404001)
  end

  error 403 do
    json error_response("Youre not authorized for this resource.", 403001)
  end

  error do
    json error_response("Internal error occured", 500001)
  end

  def invalid_login_attempt
    json error_response("Error with your login or password", 404001)
  end

  def error_response message, code
    {error: {message: message, code: code}}
  end
end
