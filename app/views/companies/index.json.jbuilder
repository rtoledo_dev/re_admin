json.array!(@companies) do |company|
  json.extract! company, :id, :name, :address_zipcode, :address, :address_number, :address_complement, :address_neighborhood, :address_city, :address_country, :twitter, :facebook, :google_plus, :linkedin
  json.url company_url(company, format: :json)
end
