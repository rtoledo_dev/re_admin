# encoding: utf-8
module RealEstatesHelper
  def real_estate_type_options
    RealEstate::TYPES.collect{|k,v| [v,k]}
  end

  def css_class_of_master(real_estate_image)
    "master" if real_estate_image.master
  end


  def state_options
    %w(AC AL AP AM BA CE DF ES GO MA MT MS MG PA PB PR PE PI RJ RN RS RO RR SC SP SE TO)
  end

  def image_dimenssions(file_path)
    geo = Paperclip::Geometry.from_file(file_path)
    "#{geo.width.round}x#{geo.height.round}"
  end
end
