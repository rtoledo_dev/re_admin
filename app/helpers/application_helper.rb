module ApplicationHelper
  def activity_event(event)
    case event
    when 'create'
      'Registro criado'
    when 'update'
      'Registro atualizado'
    when 'destroy'
      'Registro apagado'
    end
  end
end
