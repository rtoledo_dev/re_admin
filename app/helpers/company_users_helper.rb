module CompanyUsersHelper
  def companies_options
    current_company.all_companies.collect{|t| [t.full_name, t.id]}
  end

  def last_activities_for_user(user)
    Activity.order(:created_at).where(whodunnit: user.id).limit(10)
  end

  def user_type_options
    User::TYPES.collect{|k,v| [v,k]}
  end
end
