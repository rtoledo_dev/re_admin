module CompaniesHelper
  def file_hint(file_name)
    return nil if file_name.blank?
    "Arquivo atual <u>#{file_name}</u>".html_safe
  end
end
