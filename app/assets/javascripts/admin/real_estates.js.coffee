$ ->
  $('.real_estate_tabs').tab()
  $('.real_estate_tabs').bind "show", (e) ->
    now_tab = $(e.target)
    if now_tab.data('ajax-source') != undefined
      $(now_tab.attr('href')).html('')
      $.get(now_tab.data('ajax-source'))
  if $('#real_estate_sold').is(':checked')
    $('.sold_info').show()
  else
    $('.sold_info').hide()

  $('#real_estate_sold_at').datepicker({format: "dd/mm/yyyy"})
  $('#real_estate_sold').bind "change", ->
    $('.sold_info input').val('')
    if $('#real_estate_sold').is(':checked')
      $('.sold_info').show()
    else
      $('.sold_info').hide()
