/* [ ---- Gebo Admin Panel - extended form elements ---- ] */

	$(document).ready(function() {
		//* masked inputs
		gebo_mask_input.init();
		//* datepicker
		// gebo_datepicker.init();
		// gebo_timepicker.init();
		gebo_multiselect.init();
    gebo_spinners.init();
		
		$('.open_modal_form').click(function(e) {
			$.colorbox({
				href: '#modal_form',
				inline: true,
				opacity: '0.2',
				fixed: true,
				scrolling: false
			});
			e.preventDefault();
		})
		
	});
	
	//* masked input
	gebo_mask_input = {
		init: function() {
			$("input[name*='phone']").inputmask("(99) 9999-9999[9]");
			$("input[name*='zipcode']").inputmask("99999-999");
			$(".currency").maskMoney({showSymbol:false, decimal:',',thousands:'.'});
		}
	};
	
	//* bootstrap datepicker
	gebo_datepicker = {
		init: function() {
			$('#dp1').datepicker();
			$('#dp2').datepicker();
			
			$('#dp_start').datepicker({format: "mm/dd/yyyy"}).on('changeDate', function(ev){
				var dateText = $(this).data('date');
				
				var endDateTextBox = $('#dp_end input');
				if (endDateTextBox.val() != '') {
					var testStartDate = new Date(dateText);
					var testEndDate = new Date(endDateTextBox.val());
					if (testStartDate > testEndDate) {
						endDateTextBox.val(dateText);
					}
				}
				else {
					endDateTextBox.val(dateText);
				};
				$('#dp_end').datepicker('setStartDate', dateText);
				$('#dp_start').datepicker('hide');
			});
			$('#dp_end').datepicker({format: "mm/dd/yyyy"}).on('changeDate', function(ev){
				var dateText = $(this).data('date');
				var startDateTextBox = $('#dp_start input');
				if (startDateTextBox.val() != '') {
					var testStartDate = new Date(startDateTextBox.val());
					var testEndDate = new Date(dateText);
					if (testStartDate > testEndDate) {
						startDateTextBox.val(dateText);
					}
				}
				else {
					startDateTextBox.val(dateText);
				};
				$('#dp_start').datepicker('setEndDate', dateText);
				$('#dp_end').datepicker('hide');
			});
			$('#dp_modal').datepicker();
		}
	};
	
	//* bootstrap timepicker
	gebo_timepicker = {
		init: function() {
			$('#tp_1').timepicker({
				defaultTime: 'current',
				minuteStep: 10,
				disableFocus: true,
				template: 'modal',
				showMeridian: false
			});
			$('#tp_2').timepicker({
				defaultTime: 'current',
				minuteStep: 1,
				disableFocus: true,
				template: 'dropdown'
			});
			$('#tp_modal').timepicker({
				defaultTime: 'current',
				minuteStep: 1,
				disableFocus: true,
				template: 'dropdown'
			});
		}
	};
	
	//* textarea limiter
	gebo_limiter = {
		init: function(){
			$("#txtarea_limit_chars").counter({
				goal: 120
			});
			$("#txtarea_limit_words").counter({
				goal: 20,
				type: 'word'
			});
		}
	};
	
	//* textarea autosize
	gebo_auto_expand = {
		init: function() {
			$('#auto_expand').autosize();
		}
	};

	//* spinners
	gebo_spinners = {
		init: function() {
			$(".sp_basic").spinner();
		}
	};
    
    //* uniform
    gebo_uniform = {
		init: function() {
            $(".uni_style").uniform();
        }
    };
	
	//* progressbars
	gebo_progressbars = {
		init: function(){
			var iEnd1 = new Date().setTime(new Date().getTime() + 25 * 1000); // now plus 25 secs
			$('#progress1').anim_progressbar({
				finish: iEnd1,
				callback: function() {
					$.sticky("Progressbar no 1 callback", {autoclose : false, position: "top-right", type: "st-info" });
				}
			});
			var iNow = new Date().setTime(new Date().getTime() + 2 * 1000); // now plus 2 secs
			var iEnd2 = new Date().setTime(new Date().getTime() + 10 * 1000); // now plus 10 secs
			$('#progress2').anim_progressbar({
				start: iNow,
				finish: iEnd2,
				interval: 100,
				callback: function() {
					$.sticky("Progressbar no 2 callback", {autoclose : false, position: "top-right", type: "st-success" });
				}
			});
			var iEnd3 = new Date().setTime(new Date().getTime() + 15 * 1000); // now plus 15 secs
			$('#progress3').anim_progressbar({
				interval: 1000,
				finish: iEnd3,
				callback: function() {
					$.sticky("Progressbar no 3 callback", {autoclose : false, position: "top-right", type: "st-error" });
				}
			});
		}
	};

	//* sliders
	gebo_sliders = {
		init: function(){
			//* default slider
			$( ".ui_slider1" ).slider({
				value:100,
				min: 0,
				max: 500,
				step: 50,
				slide: function( event, ui ) {
					$( ".ui_slider1_val" ).text( "$" + ui.value );
					$( "#ui_slider_default_val" ).val( "$" + ui.value );
				}
			});
			$( ".ui_slider1_val" ).text( "$" + $( ".ui_slider1" ).slider( "value" ) );
			$( "#ui_slider_default_val" ).val( "$" + $( ".ui_slider1" ).slider( "value" ) );
		}
	};
	
	//* multiselect
	gebo_multiselect = {
		init: function(){
			
			$('.multiselect').multiSelect({
				selectableHeader	: '<h4>Opções</h4>',
				selectedHeader		: '<h4>Opções escolhidas</h4>'
			});
			$('#ms-optgroup .ms-selectable, #ms-outsideCountries .ms-selectable').find('li.ms-elem-selectable').hide();
			$('.ms-optgroup-label').click(function(){
				if ($(this).hasClass('ms-collapse')){
					$(this).nextAll('li').hide();
					$(this).removeClass('ms-collapse'); 
				} else {
					$(this).nextAll('li:not(.ms-selected)').show();
					$(this).addClass('ms-collapse');
				}
			});
		  
			$('#searchable-form').multiSelect({
				selectableHeader : '<input type="text" id="multi_search" autocomplete="off" placeholder="search" />',
				selectedHeader	 : '<a href="javascript:void(0)" id="sForm_deselect" class="btn">Remover todos</a>'
			});
		
			$('input#multi_search').quicksearch('#ms-searchable-form .ms-selectable li');
			$('#searchable-form').multiSelect();
			
			$('#select_all').on('click', function(){
				$('.multiselect').multiSelect('select_all');
				return false;
			});
			
			$('#deselect_all').on('click', function(){
				$('.multiselect').multiSelect('deselect_all');
				return false;
			});
			
			$('#sForm_deselect').on('click', function(){
				$('#searchable-form').multiSelect('deselect_all');
				return false;
			});

		}
	};
	
	//* enhanced select elements
	gebo_chosen = {
		init: function(){
			$(".chzn_a").chosen({
				allow_single_deselect: true
			});
			$(".chzn_b").chosen();
		}
	};
    
	//* drag&drop multi-upload
  gebo_multiupload = {
    init: function() {
    $("#multi_upload").pluploadQueue({
        // General settings
        runtimes : 'html5,flash,silverlight',
        url : '/plupload/examples/upload.php',
        max_file_size : '10mb',
        chunk_size : '1mb',
        unique_names : true,
        browse_button : 'pickfiles',

        // Specify what files to browse for
        filters : [
            {title : "Arquivos de imagem", extensions : "jpg,gif,png"},
            {title : "Arquivos ZIP", extensions : "zip"}
        ],

        // Flash settings
        flash_swf_url : '/plupload/js/plupload.flash.swf',

        // Silverlight settings
        silverlight_xap_url : '/plupload/js/plupload.silverlight.xap'
      });
    }
  };
	
	//* colorpicker
	gebo_colorpicker = {
		init: function(){
			$('#cp1').colorpicker({
				format: 'hex'
			});
			$('#cp2').colorpicker();
			$('#cp3').colorpicker();
			
			$('#cp_modal').colorpicker();
		}
	};
	
	
	
	
