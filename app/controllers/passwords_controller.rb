class PasswordsController < Devise::PasswordsController
  skip_before_filter :authenticate_user!
  
  # POST /users/password
  def create
    self.resource = resource_class.send_reset_password_instructions(resource_params)
    flash[:password] ||= {}

    if successfully_sent?(resource)
      flash[:password][:notice] = t('devise.passwords.send_instructions') if request.post?
    else
      flash[:password][:error] = t('devise.passwords.error_on_send_instructions') if request.post?
    end
    resource.email = ''
  end

  # GET /users/password/edit?reset_password_token=abcdef
  def edit
    self.resource = resource_class.new
    resource.reset_password_token = params[:reset_password_token]

    render :layout => 'devise'
  end

  # PUT /users/password
  def update
    # debugger
    self.resource = resource_class.reset_password_by_token(resource_params)

    if resource.errors.empty?
      resource.unlock_access! if unlockable?(resource)

      flash[:password] ||= {}
      flash[:password][:notice] = t('devise.passwords.updated')

      sign_in(resource_name, resource)
    end

    render :edit
  end
end