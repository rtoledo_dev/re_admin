# encoding: utf-8
class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  helper_method :current_company
  before_filter :authenticate_user!

  def current_company
    return unless current_user
    current_user.master_company
  end

  def after_sign_in_path_for(resource)
    real_estates_path
  end

  def after_sign_out_path_for(resource)
    new_user_session_path
  end

  def permit_to_sys_admin
    if current_user && !current_user.sys_admin?
      redirect_to root_path, error: 'Você não tem permissão a esta área'
    end
  end
end
