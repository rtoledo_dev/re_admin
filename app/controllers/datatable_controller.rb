class DatatableController < ApplicationController
  def i18n
    locale = {
      "sProcessing" => I18n.t('datatable.sProcessing'),
      "sLengthMenu" => I18n.t('datatable.sLengthMenu'),
      "sZeroRecords" =>  I18n.t('datatable.sZeroRecords'),
      "sInfo" =>       I18n.t('datatable.sInfo'),
      "sInfoEmpty" =>  I18n.t('datatable.sInfoEmpty'),
      "sInfoFiltered" => I18n.t('datatable.sInfoFiltered'),
      "sInfoPostFix" =>  I18n.t('datatable.sInfoPostFix'),
      "sSearch" =>     I18n.t('datatable.sSearch'),
      "sUrl" =>        I18n.t('datatable.sUrl'),
      "oPaginate" => {
        "sFirst" =>  I18n.t('datatable.sFirst'),
        "sPrevious" => I18n.t('datatable.sPrevious'),
        "sNext" =>   I18n.t('datatable.sNext'),
        "sLast" =>   I18n.t('datatable.sLast')
      }
    }

    render :json => locale
  end
end
