class RealEstatesController < ApplicationController
  add_breadcrumb :index, :real_estates_path
  before_action :set_real_estate, only: [:show, :edit, :update, :destroy,
    :upload_images, :destroy_image, :reload_images, :update_image_master]
  skip_before_filter :verify_authenticity_token, :only => [:upload_images]

  # GET /real_estates
  # GET /real_estates.json
  def index
    @real_estates = []
    if current_user.master?
      @real_estates = current_user.company.all_real_estates
    else
      @real_estates = current_user.company.real_estates
    end
  end

  # GET /real_estates/1
  # GET /real_estates/1.json
  def show
    add_breadcrumb :show, real_estate_path(@real_estate)
  end

  # GET /real_estates/new
  def new
    add_breadcrumb :new, new_real_estate_path
    @real_estate = RealEstate.new
  end

  # GET /real_estates/1/edit
  def edit
    add_breadcrumb :edit, edit_real_estate_path(@real_estate)
  end

  # POST /real_estates
  # POST /real_estates.json
  def create
    add_breadcrumb :new, new_real_estate_path
    @real_estate = RealEstate.new(real_estate_params)

    respond_to do |format|
      if @real_estate.save
        format.html { redirect_to edit_real_estate_path(@real_estate), notice: 'Informações salvas com sucesso.' }
        format.json { render action: 'show', status: :created, location: @real_estate }
      else
        format.html { render action: 'new' }
        format.json { render json: @real_estate.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /real_estates/1
  # PATCH/PUT /real_estates/1.json
  def update
    add_breadcrumb :edit, edit_real_estate_path(@real_estate)
    respond_to do |format|
      if @real_estate.update(real_estate_params)
        format.html { redirect_to @real_estate, notice: 'Informações salvas com sucesso.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @real_estate.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /real_estates/1
  # DELETE /real_estates/1.json
  def destroy
    @real_estate.destroy
    respond_to do |format|
      format.html { redirect_to real_estates_url }
      format.json { head :no_content }
    end
  end

  def reload_images
    respond_to do |format|
      format.js
    end
  end

  def update_image_master
    real_estate_image = @real_estate.real_estate_images.find(params[:real_estate_image_id])
    if real_estate_image
      @real_estate.real_estate_images.update_all(master: false)
      real_estate_image.master = true
      real_estate_image.save
      respond_to do |format|
        format.js { render action: 'reload_images'}
      end
    else
      render nothing: true
    end
  end

  def upload_images
    real_estate_image = @real_estate.real_estate_images.build(image: params[:file])
    real_estate_image.save!
    render nothing: true
  end

  def destroy_image
    real_estate_image = @real_estate.real_estate_images.find(params[:real_estate_image_id])
    respond_to do |format|
      if real_estate_image && real_estate_image.destroy
        format.js { render action: 'reload_images'}
      else
        format.js { render nothing: true}
      end
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_real_estate
      @real_estate = nil
      if current_user.master?
        @real_estate = current_user.company.all_real_estates.find(params[:id])
      else
        @real_estate = current_user.company.real_estates.find(params[:id])
      end
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def real_estate_params
      _params = params.require(:real_estate).permit(:real_estate_type, :title,
        :short_description, :description, :address_zipcode, :address, :address_number,
        :address_complement, :address_neighborhood, :address_reference,
        :address_region, :address_city, :address_state, :address_country,
        :selling_price, :comission_price, :sold, :active, :owner_name,
        :owner_phone, :owner_mobile_phone, :owner_agent_name,
        :owner_agent_phone, :owner_agent_mobile_phone, :gated, :rooms,
        :bathrooms, :terrain_area, :build_area, :fireplace, :pool, :bbq_area,
        :kennel, :dependencies, :homemade_house, :turkish_bath, :game_room,
        :sold_price, :sold_at, real_estate_specifications_attributes:[
          :value, :specification, :specification_id, :id, :_destroy])
      _params.merge({company: current_user.company, goal_ids: params[:real_estate][:goal_ids]})
    end
end
