# encoding: utf-8
class CompanyUsersController < ApplicationController
  add_breadcrumb :index, :company_users_path
  before_action :set_user, only: [:show, :edit, :update, :destroy]
  before_filter :permit_to_master

  def permit_to_master
    if !current_user.sys_admin? || !current_user.master?
      redirect_to root_path, error: 'Você não tem permissão a esta área'
    end
  end

  # GET /users
  # GET /users.json
  def index
    @users = current_company.all_users
  end

  # GET /users/1
  # GET /users/1.json
  def show
    add_breadcrumb :show, company_user_path(@user)
  end

  # GET /users/new
  def new
    @user = User.new
    add_breadcrumb :new, new_company_user_path
  end

  # GET /users/1/edit
  def edit
    add_breadcrumb :edit, edit_company_user_path(@user)
  end

  # POST /users
  # POST /users.json
  def create
    add_breadcrumb :new, new_company_user_path
    @user = User.new(user_params)

    respond_to do |format|
      if @user.save
        format.html { redirect_to company_users_path, notice: 'Informações salvas com sucesso.' }
        format.json { render action: 'show', status: :created, location: @user }
      else
        format.html { render action: 'new' }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /users/1
  # PATCH/PUT /users/1.json
  def update
    add_breadcrumb :edit, edit_company_user_path(@user)
    clean_user_params = user_params
    if clean_user_params[:password].blank?
      clean_user_params.delete(:password)
      clean_user_params.delete("password")
      clean_user_params.delete(:password_confirmation)
      clean_user_params.delete("password_confirmation")
    end

    respond_to do |format|
      if @user.update(clean_user_params)
        format.html { redirect_to company_users_path, notice: 'Informações salvas com sucesso.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /users/1
  # DELETE /users/1.json
  def destroy
    @user.destroy
    respond_to do |format|
      format.html { redirect_to company_users_path }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_user
      @user = current_company.all_users.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def user_params
      _params = params.require(:user).permit(:name, :email, :password,
        :password_confirmation, :master, :phone, :mobile_phone, :user_type,
        user_details_attributes:[:value, :detail, :detail_id, :id, :_destroy])
      _params = _params.except(:master) unless current_user.master?
      if params[:user] && params[:user][:company_id]
        company = current_company.all_companies.find_by_id(params[:user][:company_id])
      end
      _params.merge({master_company_id: current_company.id, company_id: company.try(:id)})
    end
end
