class RegistrationsController < Devise::RegistrationsController
  def update
    self.resource = resource_class.to_adapter.get!(send(:"current_#{resource_name}").to_key)
    prev_unconfirmed_email = resource.unconfirmed_email if resource.respond_to?(:unconfirmed_email)

    clean_account_update_params = params.require(:user).permit(:name, :email, :password,
          :password_confirmation, :current_password)
    if update_resource(resource, clean_account_update_params)
      if is_navigational_format?
        flash_key = update_needs_confirmation?(resource, prev_unconfirmed_email) ?
          :update_needs_confirmation : :updated
      end

      flash[:notice] = 'Informações atualizadas com sucesso'
      sign_in resource_name, resource, :bypass => true
      respond_to do |format|
        format.js
      end
    else
      clean_up_passwords resource
      @user = resource
      respond_to do |format|
        format.js { render "error"}
      end
    end
  end
end