Warden::Strategies.add(:api_authenticable) do
  def valid?
    params["email"] || params["password"]
  end

  def authenticate!
    user = User.find_by_email(params["email"])
    if user && user.valid_password?(params["password"])
      success!(user)
    else
      fail!("Could not log in")
    end
  end
end